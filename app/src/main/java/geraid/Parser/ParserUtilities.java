package geraid.Parser;

import java.io.IOException;

public class ParserUtilities {
    public static void startReaderUI(String loc, String settings){
        ProcessBuilder pb = new ProcessBuilder(loc, "--settings:"+settings);

        try {
            pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
